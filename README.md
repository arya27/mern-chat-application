This is a full fledged Web chat application, where a user can fo following:

1. Create account
2. Login to account (credential login, auto login using token)
3. See other online users
4. Chat with each other
5. Chat history will be persisted
6. User can setup his profile like Display Pic, About, email id, change password, Name change, phone number, delete account etc
7. User can see other party basic profile like Name, email, about, display pic, etc